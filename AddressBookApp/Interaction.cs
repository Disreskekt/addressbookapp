﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBookApp
{
    static class Interaction
    {
        public static int Greeting()
        {
            Console.WriteLine("\nДобро пожаловать в записную книгу.\nЗдесь вы можете:\n1.Создать новую запись.\n2.Редактировать запись.\n3.Удалить запись.\n4.Посмотреть запись.\n5.Посмотреть все записи.\nДля завершения программы введите 0");
            return Choose();
        }
        public static int Choose()
        {
            Console.WriteLine("Выберите номер нужного действия: ");
            string temp = Console.ReadLine();
            int temp2;
            if (Int32.TryParse(temp, out temp2))
            {
                if (temp2 < 0 || temp2 > 5)
                {
                    Console.WriteLine("Такого номера не существует. Попробуйте еще раз.");
                    return Choose();
                }
                else
                {
                    return temp2;
                }
            }
            else
            {
                Console.WriteLine("Нужно ввести цифру! Попробуйте еще раз.");
                return Choose();
            }
        }
        public static void Input()
        {
            Console.WriteLine("Обязательные поля помечены звездочкой.");
        }
        public static string InputSurName()
        {
            Console.WriteLine("*Введите фамилию: ");
            string temp = Console.ReadLine();
            if (temp.Trim(' ') == "")
            {
                Console.WriteLine("Это обязательное поле! Напишите что-нибудь.");
                return InputSurName();
            }
            else
            {
                return temp;
            }
        }
        public static string InputFirstName()
        {
            Console.WriteLine("*Имя: ");
            string temp = Console.ReadLine();
            if (temp.Trim(' ') == "")
            {
                Console.WriteLine("Это обязательное поле! Напишите что-нибудь.");
                return InputFirstName();
            }
            else
            {
                return temp;
            }
        }
        public static string InputPatronymic()
        {
            Console.WriteLine("Отчество: ");
            return Console.ReadLine();
        }
        public static int InputPhoneNumber()
        {
            Console.WriteLine("*Номер телефона(без кода): ");
            string temp = Console.ReadLine();
            int temp2;
            if (temp.Trim(' ') == "")
            {
                Console.WriteLine("Это обязательное поле! Напишите что-нибудь.");
                return InputPhoneNumber();
            }
            else if (Int32.TryParse(temp, out temp2))
            {
                return temp2;
            }
            else
            {
                Console.WriteLine("Тут должны быть цифры! Попробуйте еще раз.");
                return InputPhoneNumber();
            }
        }
        public static string InputCountry()
        {
            Console.WriteLine("*Страна: ");
            string temp = Console.ReadLine();
            if (temp.Trim(' ') == "")
            {
                Console.WriteLine("Это обязательное поле! Напишите что-нибудь.");
                return InputCountry();
            }
            else
            {
                return temp;
            }
        }
        public static DateTime InputDateOfBirth()
        {
            Console.WriteLine("Дата рождения: ");
            string temp = Console.ReadLine();
            DateTime temp2;
            if (DateTime.TryParse(temp, out temp2) || temp == "")
            {
                return temp2;
            }
            else
            {
                Console.WriteLine("Что-то пошло не так. Попробуйте еще раз.");
                return InputDateOfBirth();
            }
        }
        public static string InputOrganization()
        {
            Console.WriteLine("Организация: ");
            return Console.ReadLine();
        }
        public static string InputPost()
        {
            Console.WriteLine("Должность: ");
            return Console.ReadLine();
        }
        public static string InputOtherNotes()
        {
            Console.WriteLine("Заметки: ");
            return Console.ReadLine();
        }
        public static int ChoosePole()
        {
            Console.WriteLine("\nКакое поле вы хотите изменить?");
            Console.WriteLine("1.Фамилия\n" +
                "2.Имя\n" +
                "3.Отчество\n" +
                "4.Номер телефона\n" +
                "5.Страна\n" +
                "6.Дата рождения\n" +
                "7.Организация\n" +
                "8.Должность\n" +
                "9.Заметки\n");
            string temp = Console.ReadLine();
            int temp2;
            if (Int32.TryParse(temp, out temp2))
            {
                return temp2;
            }
            else
            {
                Console.WriteLine("Тут должна быть цифра!");
                return ChoosePole();
            }
        }
        public static void SuccessDelete()
        {
            Console.WriteLine("Запись успешно удалена!");
        }
        public static int ChooseNote()
        {
            Console.WriteLine("Введите номер контакта: ");
            string temp = Console.ReadLine();
            int temp2;
            if (Int32.TryParse(temp, out temp2))
            {
                return temp2;
            }
            else
            {
                Console.WriteLine("Тут должна быть цифра!");
                return ChooseNote();
            }
        }
        public static void NoteDoesntExist()
        {
            Console.WriteLine("Такого контакта не существует!");
        }
        public static void BookIsEmpty()
        {
            Console.WriteLine("Записная книга пуста!");
        }
        public static void WrongPole()
        {
            Console.WriteLine("Поля с таким номером не существует!");
        }
    }
}
