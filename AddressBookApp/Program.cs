﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AddressBookApp
{
    class AddressBook
    {
        private static int act;
        private static List<Note> addressBook = new List<Note>();
        static void Main(string[] args)
        {
            while (true)
            {
                act = Interaction.Greeting();
                if (act == 0)
                {
                    break;
                }
                switch (act)
                {
                    case 1:
                        CreateNewNote();
                        break;
                    case 2:
                        EditNote();
                        break;
                    case 3:
                        DeleteNote();
                        break;
                    case 4:
                        ReadNote();
                        break;
                    case 5:
                        ShowAllNotes();
                        break;
                    default:

                        break;
                }
            }
        }
        public static void CreateNewNote()
        {
            Note tempNote = new Note();
            Interaction.Input();
            tempNote.SurName = Interaction.InputSurName();
            tempNote.FirstName = Interaction.InputFirstName();
            tempNote.Patronymic = Interaction.InputPatronymic();
            tempNote.PhoneNumber = Interaction.InputPhoneNumber();
            tempNote.Country = Interaction.InputCountry();
            tempNote.DateOfBirth = Interaction.InputDateOfBirth();
            tempNote.Organization = Interaction.InputOrganization();
            tempNote.Post = Interaction.InputPost();
            tempNote.OtherNotes = Interaction.InputOtherNotes();
            addressBook.Add(tempNote);
        }
        public static void EditNote()
        {
            int noteNumberToEdit = Interaction.ChooseNote() - 1;
            if (noteNumberToEdit < addressBook.Count && noteNumberToEdit >= 0)
            {
                int poleNumberToEdit = Interaction.ChoosePole() - 1;
                switch (poleNumberToEdit)
                {
                    case 0:
                        addressBook[noteNumberToEdit].SurName = Interaction.InputSurName();
                        break;
                    case 1:
                        addressBook[noteNumberToEdit].FirstName = Interaction.InputFirstName();
                        break;
                    case 2:
                        addressBook[noteNumberToEdit].Patronymic = Interaction.InputPatronymic();
                        break;
                    case 3:
                        addressBook[noteNumberToEdit].PhoneNumber = Interaction.InputPhoneNumber();
                        break;
                    case 4:
                        addressBook[noteNumberToEdit].Country = Interaction.InputCountry();
                        break;
                    case 5:
                        addressBook[noteNumberToEdit].DateOfBirth = Interaction.InputDateOfBirth();
                        break;
                    case 6:
                        addressBook[noteNumberToEdit].Organization = Interaction.InputOrganization();
                        break;
                    case 7:
                        addressBook[noteNumberToEdit].Post = Interaction.InputPost();
                        break;
                    case 8:
                        addressBook[noteNumberToEdit].OtherNotes = Interaction.InputOtherNotes();
                        break;
                    default:
                        Interaction.WrongPole();
                        break;
                }
            }
            else
            {
                Interaction.NoteDoesntExist();
            }
        }
        public static void DeleteNote()
        {
            int noteNumberToDelete = Interaction.ChooseNote() - 1;
            if (noteNumberToDelete < addressBook.Count && noteNumberToDelete >= 0)
            {
                addressBook.RemoveAt(noteNumberToDelete);
                Interaction.SuccessDelete();
            }
            else
            {
                Interaction.NoteDoesntExist();
            }
        }
        public static void ReadNote()
        {
            int noteNumberToView = Interaction.ChooseNote() - 1;
            if (noteNumberToView < addressBook.Count && noteNumberToView >= 0)
            {
                Console.WriteLine("\n");
                Console.WriteLine(addressBook[noteNumberToView].SurName);
                Console.WriteLine(addressBook[noteNumberToView].FirstName);
                Console.WriteLine(addressBook[noteNumberToView].Patronymic);
                Console.WriteLine(addressBook[noteNumberToView].PhoneNumber);
                Console.WriteLine(addressBook[noteNumberToView].Country);
                Console.WriteLine(addressBook[noteNumberToView].DateOfBirth);
                Console.WriteLine(addressBook[noteNumberToView].Organization);
                Console.WriteLine(addressBook[noteNumberToView].Post);
                Console.WriteLine(addressBook[noteNumberToView].OtherNotes);
            }
            else
            {
                Interaction.NoteDoesntExist();
            }
        }
        public static void ShowAllNotes()
        {
            if (addressBook.Count > 0)
            {
                for (int i = 0; i < addressBook.Count; i++)
                {
                    Console.WriteLine("\n" + (i + 1) + ")");
                    Console.WriteLine(addressBook[i].SurName);
                    Console.WriteLine(addressBook[i].FirstName);
                    Console.WriteLine(addressBook[i].PhoneNumber);
                }
            }
            else
            {
                Interaction.BookIsEmpty();
            }
        }
    }
}
